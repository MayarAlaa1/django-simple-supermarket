# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Customer(models.Model):
    customer_id = models.AutoField(primary_key=True)
    user = models.OneToOneField(User,on_delete=models.CASCADE ,null=True,blank=True)
    name = models.CharField(max_length=30, null=True)
    phone = models.CharField(max_length=11, null=True)
    email = models.EmailField()
    address = models.CharField(max_length=50, null=True)
    block_state = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Product(models.Model):
    product_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30,null=False)
    price = models.FloatField()
    available_quantity = models.IntegerField(default=0,null=True,blank=True)

    def __str__(self):
        return self.name

class Invoice(models.Model):
    invoice_id = models.AutoField(primary_key=True)
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL,blank=True,null=True)
    products = models.ManyToManyField('Product')
    shipping_address = models.CharField(max_length=250, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.invoice_id)
