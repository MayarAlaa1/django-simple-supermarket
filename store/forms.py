from django import forms
from .models import Invoice

class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ('customer', 'products', 'shipping_address')
        widgets = {
                    'customer': forms.Select( attrs={'class': 'form-control '}),
                    'products': forms.CheckboxSelectMultiple(),
                    'shipping_address': forms.TextInput( attrs={'class': 'form-control ', 'placeholder':'Shipping Address'}),
        }
