# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.template.loader import get_template
from django.shortcuts import render,get_object_or_404
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from .models import *
from .forms import InvoiceForm
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse
from django.template.loader import render_to_string
from weasyprint import HTML
import tempfile

# Create your views here.

def products (request):
    products = Product.objects.all()
    context = {'products':products}
    return render(request,'store/products.html',context)

def customers (request):
    customers = Customer.objects.all()
    context = {'customers':customers}
    return render(request,'store/customers.html',context)


def customer_details(request,c_id):
    customer = get_object_or_404(Customer,customer_id=c_id)
    invoices = Invoice.objects.filter(customer=c_id)
    context = {'customer':customer,
                'invoices':invoices
    }
    
    return render(request,'store/customer.html', context)

def invoice_details(request,invoice_id):
    
    invoice = get_object_or_404(Invoice,invoice_id=invoice_id)
    products = invoice.products.all()
    context = {'invoice':invoice,
                'products':products
    }
    
    return render(request,'store/invoice.html', context)

@staff_member_required
def addInvoice(request):
	invoice_form = InvoiceForm()
	if(request.method == 'POST'):
		invoice_form = InvoiceForm(request.POST)
		if invoice_form.is_valid():
			invoice_form.save()
            # send_mail('subject','message','from','to',fail_silently=False) get the to mail from https://temp-mail.org/ email should be replaced with customer.email
			send_mail('Hello from django supermarket','Hello we would like to inform you that your invoice is created','mayar.alaa188@gmail.com',['kinofo9406@jalcemail.com'],fail_silently=False)
			return HttpResponseRedirect('/products')
	else:
		context = {'invoice_form': invoice_form}
		return render(request,'store/invoice_add.html', context)


def pdf (request):
    # invoice = get_object_or_404(Invoice,invoice_id=pdf_id)
    html_string = render_to_string('store/pdf.html',{'invoice':[]})
    html = HTML(string=html_string)
    result = html.write_pdf()
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=invoice.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'r',newline='', encoding='ISO-8859-1')
        response.write(output.read())

    return response
