from django.conf.urls import url
from . import views
# from wkhtmltopdf.views import PDFTemplateView

urlpatterns = [
	#Leave as empty string for base url
	
	url('products', views.products, name="products"),
	url('addInvoice',views.addInvoice, name="addInvoice"),
	url('customers/', views.customers, name="customers"),
	url(r'^customer/(?P<c_id>\d+)/$', views.customer_details,name="customer"),
	url(r'^invoice/(?P<invoice_id>\d+)/$', views.invoice_details,name="invoice"),
	url('pdf',views.pdf,name="pdf")
]