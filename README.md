# **Django Simple Supermarket**

# Installation
### To try the project locally

1- clone the project
git clone https://MayarAlaa1@bitbucket.org/MayarAlaa1/django-simple-supermarket.git

2- open the terminal in the project's location

3- install all the project requirements 

pip install requirements.txt

4- run the migrations on your machine

python3 manage.py makemigrations
python3 manage.py migrate

5- create a superuser to be able to manage the blog (using django admin panel)

python3 manage.py createsuperuser and enter the username,email and password of the admin

6- run the server locally on your machine

python3 manage.py runserver

7- using allauth authentication package
signup url : /accouts/signup
login url : /accouts/login

